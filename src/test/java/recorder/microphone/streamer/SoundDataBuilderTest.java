package recorder.microphone.streamer;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;

import recorder.common.exception.DataConsistencyException;
import recorder.microphone.app.SoundDataBuilder;

public class SoundDataBuilderTest {

	@Test
	public void buildSoundDataTestComplete() throws DataConsistencyException {
		
		//given
		Instant time = Instant.ofEpochMilli(0);
		byte[] sound = new byte[] {1};
		String format = "s32be";
		byte[] correctData = new byte[] {0x12, 0x34, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1};
		SoundDataBuilder builder = new SoundDataBuilder();
		
		//when
		builder.withData(sound).withFormat(format).withTimestamp(time);
		
		//then
		byte[] builtData = builder.buildSoundData();
		
		Assert.assertArrayEquals(correctData, builtData);
	}
	
	@Test(expected = DataConsistencyException.class)
	public void buildSoundDataTestNoData() throws DataConsistencyException {
		//given
		SoundDataBuilder builder = new SoundDataBuilder();
		//when

		//then
		builder.buildSoundData();
	}
}
