package recorder.microphone.app;

import java.time.Instant;

public class AudioContainer {
	
	private byte[] mBufferForMicrophone;
	
	private byte[] mBufferForData;
	
	private Instant mTimestamp;

	public AudioContainer() {
		float bufferSampleSizeConstant = (float)Configuration.getSizeInBits() / Byte.SIZE * Configuration.getsSampleRate();
		
		if(Configuration.getInterval() == Configuration.getSampleDuration()) {
			mBufferForMicrophone = new byte[(int) (bufferSampleSizeConstant * Configuration.getSampleDuration())];
			mBufferForData = mBufferForMicrophone;
		} else {
			mBufferForMicrophone = new byte[(int) (bufferSampleSizeConstant * Configuration.getInterval())];
			mBufferForData = new byte[(int) (bufferSampleSizeConstant * Configuration.getSampleDuration())];
		}
	}
		
	public byte[] bufferForMicrophone() {
		return mBufferForMicrophone;
	}

	public byte[] bufferForData() {
		return mBufferForData;
	}

	public Instant getTimestamp() {
		return mTimestamp;
	}
	
	public void setTimestamp(Instant timestamp) {
		this.mTimestamp = timestamp;
	}
}
