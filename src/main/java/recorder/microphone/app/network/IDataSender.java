package recorder.microphone.app.network;

public interface IDataSender {

	byte[] sendRequest(byte[] data);
	
}
