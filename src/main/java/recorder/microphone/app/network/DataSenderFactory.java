package recorder.microphone.app.network;

import java.io.IOException;

import org.apache.log4j.Logger;

public class DataSenderFactory {
	
	private final static Logger LOGGER = Logger.getLogger(DataSenderFactory.class);
	
	public static IDataSender getDataSender() {
		try {
			return new SocketDataSender();
		} catch (IOException e) {
			LOGGER.error("Could not connect to the server. Recorded data will not be sent.");
		}
		return null;
	}
	
}
