package recorder.microphone.app.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import recorder.microphone.app.Configuration;

public class SocketDataSender extends Socket implements IDataSender {

	private final static Logger LOGGER = Logger.getLogger(SocketDataSender.class);
	
	public SocketDataSender() throws UnknownHostException, IOException {
		super(Configuration.getServerAddress(), Configuration.getServerPort());
	}
	
	@Override
	public byte[] sendRequest(byte[] data) {
		try {
			OutputStream outputServer = getOutputStream();
			InputStream inputFromServer = getInputStream();
			DataOutputStream out = new DataOutputStream(outputServer);
			out.write(data);
			shutdownOutput();
			DataInputStream in = new DataInputStream(inputFromServer);
			byte[] answer = new byte[Configuration.getMagicNumber().length + Long.BYTES];
			in.read(answer);
			return answer;
		} catch (IOException e) {
			LOGGER.error("Communication client-server failed.");
		}
		return new byte[0];
	}

}
