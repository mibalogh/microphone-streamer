package recorder.microphone.app;

import org.apache.log4j.Logger;

import recorder.common.listener.KeyExitListener;

public class AppClient {
	
	private static final Logger LOGGER = Logger.getLogger(AppClient.class);
	
    public static void main( String[] args ) throws InterruptedException {
    	CliParser parser = new CliParser(args);
    	parser.parse();
    	RecordingService service = new RecordingService();
    	KeyExitListener.addExitKeyListener(service);
    	LOGGER.info("Stream initialized.");
    	service.streamSound();
    }
}
