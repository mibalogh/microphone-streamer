package recorder.microphone.app.strategy;

import java.time.Instant;

import recorder.microphone.app.AudioContainer;
import recorder.microphone.app.Configuration;

public class LongerDurationBufferStrategy implements IFillDataBufferStrategy {

	@Override
	public byte[] fillDataBuffer(AudioContainer bufferContainer) {
		int length = bufferContainer.bufferForData().length - bufferContainer.bufferForMicrophone().length;
		System.arraycopy(bufferContainer.bufferForData(), bufferContainer.bufferForMicrophone().length, bufferContainer.bufferForData(), 0, length);
		System.arraycopy(bufferContainer.bufferForMicrophone(), 0, bufferContainer.bufferForData(), length, bufferContainer.bufferForMicrophone().length);
		return bufferContainer.bufferForData();
	}

	@Override
	public Instant recordedDataTimestamp(AudioContainer bufferContainer) {
		Instant timestampMicrophoneRecord = bufferContainer.getTimestamp();
		float samplesToShift = bufferContainer.bufferForData().length - bufferContainer.bufferForMicrophone().length;
		return timestampMicrophoneRecord.minusNanos((long) (samplesToShift * Configuration.getSampleDurationInNanos()));
	}

}
