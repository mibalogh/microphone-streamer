package recorder.microphone.app.strategy;

import java.time.Instant;

import recorder.microphone.app.AudioContainer;
import recorder.microphone.app.Configuration;

public class LongerIntervalBufferStrategy implements IFillDataBufferStrategy {

	@Override
	public byte[] fillDataBuffer(AudioContainer bufferContainer) {
		System.arraycopy(bufferContainer.bufferForMicrophone(), 
				bufferContainer.bufferForMicrophone().length - bufferContainer.bufferForData().length,
				bufferContainer.bufferForData(), 0, bufferContainer.bufferForData().length);
		return bufferContainer.bufferForData();
	}

	@Override
	public Instant recordedDataTimestamp(AudioContainer bufferContainer) {
		Instant timestampMicrophoneRecord = bufferContainer.getTimestamp();
		float samplesToShift = bufferContainer.bufferForMicrophone().length - bufferContainer.bufferForData().length;
		return timestampMicrophoneRecord.plusNanos((long) (samplesToShift * Configuration.getSampleDurationInNanos()));
	}


}
