package recorder.microphone.app.strategy;

import java.time.Instant;

import recorder.microphone.app.AudioContainer;

public interface IFillDataBufferStrategy {
	
	byte[] fillDataBuffer(AudioContainer bufferContainer);
	
	Instant recordedDataTimestamp(AudioContainer bufferContainer);
}
