package recorder.microphone.app.strategy;

import recorder.microphone.app.Configuration;

public class BufferStrategyFactory {

	public static IFillDataBufferStrategy getBufferStrategy() {
		
		if(Configuration.getSampleDuration() > Configuration.getInterval()) {
			return new LongerDurationBufferStrategy();
		} else if(Configuration.getSampleDuration() < Configuration.getInterval()) {
			return new LongerIntervalBufferStrategy();
		} else {
			return new DefaultFillBufferStrategy();
		}
	}
}
