package recorder.microphone.app.strategy;

import java.time.Instant;

import recorder.microphone.app.AudioContainer;

public class DefaultFillBufferStrategy implements IFillDataBufferStrategy {

	@Override
	public byte[] fillDataBuffer(AudioContainer bufferContainer) {
		return bufferContainer.bufferForData();
	}

	@Override
	public Instant recordedDataTimestamp(AudioContainer bufferContainer) {
		return bufferContainer.getTimestamp();
	}
	
}
