package recorder.microphone.app;

import javax.sound.sampled.AudioFormat;

public class Configuration {
	
	private static final byte[] MAGIC_NUMBER = new byte[] {0x12, 0x34};
	
	private static final float NANOS_IN_SECOND = 1000000000.0f;

	private static float sInterval = 2.0f;
	
	private static float sSampleDuration = 4.0f;
	
	private static float sSampleRate = 16000.0f;

	private static int sSizeInBits = 16;
	
	private static int sChannels = 1;
	
	private static boolean sDataSigned = true;
	
	private static boolean sBigEndian = false;
	
	private static String sServerAddress = "127.0.0.1";

	private static int sServerPort = 9898;
	
	private static String mFormatAsString = "s16le";

	public static String getFormatAsString() {
		return mFormatAsString;
	}

	public static void setFormatAsString(String formatAsString) {
		Configuration.mFormatAsString = formatAsString;
	}

	public static float getInterval() {
		return sInterval;
	}

	public static void setInterval(float interval) {
		Configuration.sInterval = interval;
	}

	public static float getSampleDuration() {
		return sSampleDuration;
	}

	public static void setSampleDuration(float duration) {
		Configuration.sSampleDuration = duration;
	}
	
	public static void setSampleRate(float sampleRate) {
		Configuration.sSampleRate = sampleRate;
	}
	
	public static int getChannels() {
		return sChannels;
	}

	public static boolean isDataSigned() {
		return sDataSigned;
	}

	public static void setDataSigned(boolean dataSigned) {
		Configuration.sDataSigned = dataSigned;
	}

	public static boolean isBigEndian() {
		return sBigEndian;
	}

	public static void setBigEndian(boolean bigEndian) {
		Configuration.sBigEndian = bigEndian;
	}

	public static void setSizeInBits(int sizeInBits) {
		Configuration.sSizeInBits = sizeInBits;
	}

	public static void setChannels(int channels) {
		Configuration.sChannels = channels;
	}

	public static AudioFormat getAudioFormat() {
		return new AudioFormat(sSampleRate, sSizeInBits, sChannels, sDataSigned, sBigEndian);
	}

	public static float getsSampleRate() {
		return sSampleRate;
	}

	public static int getSizeInBits() {
		return sSizeInBits;
	}
	
	public static float getSampleDurationInNanos() {
		return NANOS_IN_SECOND / sSampleRate;
	}
	
	public static String getServerAddress() {
		return sServerAddress;
	}
	
	public static void setServerAddress(String serverAddress) {
		Configuration.sServerAddress = serverAddress;
	}
	
	public static int getServerPort() {
		return sServerPort;
	}
	
	public static void setServerPort(int port) {
		Configuration.sServerPort = port;
	}

	public static byte[] getMagicNumber() {
		return MAGIC_NUMBER;
	}
	
}
