package recorder.microphone.app.sound;

public class RecorderFactory {

	public static IRecorder getRecorder() {
		return new MicrophoneRecorder();
	}
}
