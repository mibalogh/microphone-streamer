package recorder.microphone.app.sound;

public interface IRecorder {

	int recordMicrophone(byte[] buffer);
	
}
