package recorder.microphone.app.sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import org.apache.log4j.Logger;

import recorder.microphone.app.Configuration;

public class MicrophoneRecorder implements IRecorder {

	private final static Logger LOGGER = Logger.getLogger(MicrophoneRecorder.class);

	private AudioFormat mAudioFormat = Configuration.getAudioFormat();
	
	DataLine.Info mInfo = new DataLine.Info(TargetDataLine.class, mAudioFormat);
	
	TargetDataLine mLine;
	
	public MicrophoneRecorder() {
		try {
			mLine = (TargetDataLine) AudioSystem.getLine(mInfo);
			mLine.open(mAudioFormat);
			mLine.start();
		} catch (LineUnavailableException e) {
			LOGGER.error("Microphone not supported. Program will close.");
			System.exit(0);
		}
	}
	
	public int recordMicrophone(byte[] buffer) {
		int count = mLine.read(buffer, 0, buffer.length);
		return count;
	}

}
