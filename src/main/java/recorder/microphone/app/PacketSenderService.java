package recorder.microphone.app;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.Arrays;

import org.apache.log4j.Logger;

import recorder.common.exception.DataConsistencyException;
import recorder.common.exception.DataTransferException;
import recorder.microphone.app.network.DataSenderFactory;
import recorder.microphone.app.network.IDataSender;
import recorder.microphone.app.strategy.IFillDataBufferStrategy;

public class PacketSenderService implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(PacketSenderService.class);

	private AudioContainer mAudioContainer;

	private IFillDataBufferStrategy mBufferFiller;

	public PacketSenderService(AudioContainer container, IFillDataBufferStrategy bufferFiller) {
		mAudioContainer = container;
		mBufferFiller = bufferFiller;
	}

	@Override
	public void run() {
		try {
			byte[] formattedData = createBytesForSending();
			sendDataToServer(formattedData);
		} catch (DataConsistencyException e) {
			LOGGER.error(e.getMessage());
		}
	}

	private byte[] createBytesForSending() throws DataConsistencyException {
		byte[] data = mBufferFiller.fillDataBuffer(mAudioContainer);
		Instant timestamp = mBufferFiller.recordedDataTimestamp(mAudioContainer);
		SoundDataBuilder builder = new SoundDataBuilder().withFormat(Configuration.getFormatAsString())
				.withTimestamp(timestamp).withData(data);
		return builder.buildSoundData();
	}

	private void sendDataToServer(byte[] data) {
		IDataSender dataSender = DataSenderFactory.getDataSender();
		if (dataSender != null) {
			byte[] response = dataSender.sendRequest(data);
			try {
				LOGGER.info("Received a correct answer from server.");
				checkAnswer(response, data);
			} catch (DataTransferException e) {
				LOGGER.error(e.getMessage());
			}
		}
	}

	private void checkAnswer(byte[] answer, byte[] correctData) throws DataTransferException {
		ByteBuffer correctAnswerBuf = ByteBuffer.allocate(Configuration.getMagicNumber().length + Long.BYTES);
		correctAnswerBuf.put(Configuration.getMagicNumber());
		correctAnswerBuf.put(
				Arrays.copyOfRange(correctData, Short.BYTES + Short.BYTES, Short.BYTES + Short.BYTES + Long.BYTES));
		if (!Arrays.equals(answer, correctAnswerBuf.array())) {
			throw new DataTransferException("Response from server does not match the sent data.");
		}
	}
}
