package recorder.microphone.app;

import java.nio.ByteBuffer;
import java.text.ParseException;
import java.time.Instant;

import recorder.common.exception.DataConsistencyException;
import recorder.common.properties.AudioFormatPropertiesReader;

public class SoundDataBuilder {

	private Instant mTimestamp;

	private byte[] mData;

	private String mFormat;

	public SoundDataBuilder withTimestamp(Instant timestamp) {
		mTimestamp = timestamp;
		return this;
	}

	public SoundDataBuilder withData(byte[] data) {
		mData = data.clone();
		return this;
	}

	public SoundDataBuilder withFormat(String format) {
		mFormat = format;
		return this;
	}

	public byte[] buildSoundData() throws DataConsistencyException {
		if(mData == null || mFormat == null || mTimestamp == null) {
			throw new DataConsistencyException("Some of data are missing. Cannot create message for sending.");
		}
		byte[] packetSize = ByteBuffer.allocate(Short.BYTES).putShort(computePacketSize()).array();
		ByteBuffer byteBuffer = ByteBuffer.allocate(
				Configuration.getMagicNumber().length + packetSize.length + Long.BYTES + Short.BYTES + mData.length);

		byteBuffer.put(Configuration.getMagicNumber());
		byteBuffer.put(packetSize);
		byteBuffer.putLong(mTimestamp.toEpochMilli());
		byteBuffer.putShort(AudioFormatPropertiesReader.getEncodedAudioFormat(mFormat));
		byteBuffer.put(mData);

		return byteBuffer.array();
	}

	private short computePacketSize() {
		int timestampSizeOf = Long.BYTES;
		int audioFormatSizeOf = Short.BYTES;
		int samplesLength = mData.length;
		return (short) (timestampSizeOf + audioFormatSizeOf + samplesLength);
	}
}
