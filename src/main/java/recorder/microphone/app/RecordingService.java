package recorder.microphone.app;

import java.time.Instant;

import org.apache.log4j.Logger;

import recorder.common.listener.IControlledService;
import recorder.microphone.app.sound.IRecorder;
import recorder.microphone.app.sound.RecorderFactory;
import recorder.microphone.app.strategy.BufferStrategyFactory;
import recorder.microphone.app.strategy.DefaultFillBufferStrategy;
import recorder.microphone.app.strategy.IFillDataBufferStrategy;

public class RecordingService implements IControlledService {

	private final static Logger LOGGER = Logger.getLogger(RecordingService.class);
	
	private IRecorder mRecorder = RecorderFactory.getRecorder();

	private AudioContainer mAudioContainer = new AudioContainer();
	
	private IFillDataBufferStrategy mBufferFiller = BufferStrategyFactory.getBufferStrategy();
	
	private boolean mIsRecording = true;

	@Override
	public boolean isRunning() {
		return mIsRecording;
	}
	
	@Override
	public void setRunning(boolean isRunning) {
		this.mIsRecording = isRunning;
		
	}

	@Override
	public void shutdown() {
		
	}

	public void streamSound() {
		LOGGER.info("Recording started.");
		mAudioContainer.setTimestamp(Instant.now());
		mRecorder.recordMicrophone(mAudioContainer.bufferForData());
		Runnable senderThread = new PacketSenderService(mAudioContainer, new DefaultFillBufferStrategy());
		new Thread(senderThread).start();
		
		while(mIsRecording) {
			mAudioContainer.setTimestamp(Instant.now());
			mRecorder.recordMicrophone(mAudioContainer.bufferForMicrophone());
			senderThread = new PacketSenderService(mAudioContainer, mBufferFiller);
			new Thread(senderThread).start();
		}
		
		LOGGER.info("Recording stopped. Program will quit.");
		
	}



}
