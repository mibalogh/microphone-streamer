package recorder.microphone.app;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.log4j.Logger;

public class CliParser {

	private static final Logger LOGGER = Logger.getLogger(CliParser.class);

	private String[] mArgs;

	private Options mOptions = new Options();

	public CliParser(String[] args) {
		mArgs = args;

		mOptions.addOption("s", "server", true, "server's IP address, default 127.0.0.1");
		mOptions.addOption("p", "port", true, "server's port, default 9898");
		mOptions.addOption("i", "interval", true,
				"interval in seconds in which sound data will be sent to the server, default 2s");
		mOptions.addOption("l", "duration", true, "duration of recorded samples, default 4s");
		mOptions.addOption("sr", "samplerate", true, "sample rate in Hz, default 16000Hz");
		mOptions.addOption("af", "format", true,
				"one of audioformats: s16be, s16le, s24be, s24le, s32be, s32le,"
				+ " s8le, s8be, u16be, u16le, u24be, u24le, u32be, u32le, u8le, u8be, default s16le");
		mOptions.addOption("ch", "channels", true, "mono or stereo, default mono");
		mOptions.addOption("h", "help", false, "shows help");
	}
	
	public void parse() {
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(mOptions, mArgs);
			
			if(cmd.hasOption("h")) {
				printHelp();
			}
			
			if(cmd.hasOption("s")) {
				String value = cmd.getOptionValue("s");
				if(InetAddressValidator.getInstance().isValidInet4Address(value)) {
					Configuration.setServerAddress(value);
					LOGGER.info("Server's IP Address set to " + value);
				} else {
					LOGGER.info("Wrong format of server's IP Address. Using default value: " + Configuration.getServerAddress());
				}
			}
			
			if(cmd.hasOption("p")) {
				String value = cmd.getOptionValue("p");
				try {
					Integer port = Integer.valueOf(value);
					Configuration.setServerPort(port);
					LOGGER.info("Server's port set to " + value);
				} catch (NumberFormatException e) {
					LOGGER.info("Wrong port number. Using default value: " + Configuration.getServerPort());
				}
			}
			
			if(cmd.hasOption("i")) {
				String value = cmd.getOptionValue("i");
				try {
					Float interval = Float.valueOf(value); 
					Configuration.setInterval(interval);
					LOGGER.info("Sending interval set to " + value);
				} catch (NumberFormatException e) {
					LOGGER.info("Wrong interval value. Using default value: " + Configuration.getInterval());
				}
			}
			
			if(cmd.hasOption("l")) {
				String value = cmd.getOptionValue("l");
				try {
					Float length = Float.valueOf(value); 
					Configuration.setSampleDuration(length);
					LOGGER.info("Sample duration set to " + value);
				} catch (NumberFormatException e) {
					LOGGER.info("Wrong sample duration value. Using default value: " + Configuration.getSampleDuration());
				}
			}
			
			if(cmd.hasOption("sr")) {
				String value = cmd.getOptionValue("sr");
				try {
					Float sampleRate = Float.valueOf(value); 
					Configuration.setSampleRate(sampleRate);
					LOGGER.info("Sample rate set to " + value);
				} catch (NumberFormatException e) {
					LOGGER.info("Wrong sample sample rate. Using default value: " + Configuration.getsSampleRate());
				}
			}
			
			if(cmd.hasOption("af")) {
				String value = cmd.getOptionValue("af");
				LOGGER.info("Server's IP Address set to " + value);
				parseAudioFormat(value);
			}
			
			if(cmd.hasOption("ch")) {
				String value = cmd.getOptionValue("ch");
				if(value.equals("mono")) {
					Configuration.setChannels(1);
				} else if(value.equals("stereo")) {
					Configuration.setChannels(2);
				} else {
					LOGGER.info("Wrong channels value. Possible channels values are mono or stereo. Recording with " + Configuration.getChannels() + " channels.");
				}
			}
		
		} catch (ParseException e) {
			LOGGER.error("Failed to parse command line arguments.", e);
		}
	}

	
	private void printHelp() {
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("microphone-streamer", mOptions);
		System.exit(0);
	}
	
	private void parseAudioFormat(String value) {
		boolean signed= Configuration.isDataSigned();
		int bits = Configuration.getSizeInBits();
		boolean bigEndian = Configuration.isBigEndian();
		try {
			value = value.toLowerCase();
			if(value.length() == 4) {
				signed = parseAudioFormatSigned(value.charAt(0));
				bits = parseAudioFormatBits(value.substring(1));
				bigEndian = parseAudioFormatEndian(value.substring(2, 4));
			} else if(value.length() == 5) {
				signed = parseAudioFormatSigned(value.charAt(0));
				bits = parseAudioFormatBits(value.substring(1,3));
				bigEndian = parseAudioFormatEndian(value.substring(3, 5));
			}
		} catch(Exception e) {
			LOGGER.info("Problem in parsing audio format. Using S16LE by default. " +  e.getMessage());
			return;
		}
		Configuration.setBigEndian(bigEndian);
		Configuration.setSizeInBits(bits);
		Configuration.setDataSigned(signed);
		Configuration.setFormatAsString(value);
	}
	
	private boolean parseAudioFormatSigned(char signed) throws ParseException {
		if (signed == 's') {
			return true;
		} else if (signed == 'u') {
			return false;
		}
		throw new ParseException("Could not parse signed indicator in audio format.");
	}
	
	private int parseAudioFormatBits(String bitsAsString) throws NumberFormatException {
		Integer bits = Integer.valueOf(bitsAsString);
		if (bits.equals(8) || bits.equals(16) || bits.equals(32) || bits.equals(64)) {
			return bits;
		}
		throw new NumberFormatException("Could not parse bits number in audio format. Possible values are 8, 16, 32, 64.");
	}
	
	private boolean parseAudioFormatEndian(String endian) throws ParseException {
		if (endian.equals("be")) {
			return true;
		} else if (endian.equals("le")) {
			return false;
		}
		throw new ParseException("Could not parse endian indicator in audio format.");
	}
}
